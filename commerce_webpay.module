<?php
/**
 * @file
 * Implements the WebPay payment gateway in Drupal Commerce checkout.
 * 
 * Currently can only process the naira; need currency codes to process others
 */

/*
 * CONTENTS

  Hooks

    commerce_webpay_menu()
    commerce_webpay_commerce_payment_method_info()

  Commerce payment API callbacks

    commerce_webpay_settings_form()
    commerce_webpay_redirect_form()

  commerce_webpay_server_url()

  commerce_webpay_process_itn()
  commerce_webpay_itn_load()
  commerce_webpay_itn_save()

 */









/**
 * Implements hook_commerce_payment_method_info().
 * 
 * This essentially registers this as a payment method.
 * This creates a ‘payment rule’.
 */
function commerce_webpay_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['commerce_webpay'] = array(
    'method_id' => 'commerce_webpay',
    'title' => t('Interswitch WebPay'),
    'display_title' => t('<p>Interswitch WebPay</p>'),
    'short_title' => t('WebPay'),
    'description' => t('Integrates with the WebPay payment gateway.'),
    // 'display_title' => t('<p>Integration with the Interswitch WebPay payment gateway.</p>'),
    'active' => TRUE,
    'offsite' => TRUE,
    'offsite_autoredirect' => TRUE,
  );

  return $payment_methods;
}









/**
 * Implements hook_menu().
 */
function commerce_webpay_menu() {
  $items = array();

  // Define the path to receive ITNs.
  $items['commerce_webpay/%/payment/return/%'] = array(
    'title' => 'Payment Complete',
    'page callback' => 'commerce_webpay_process_return',
    'page arguments' => array(1, 4),
    'access callback' => TRUE,
    // 'type' => MENU_CALLBACK,
  );

  return $items;
}









/**
 * Payment method callback: settings form.
 */
function commerce_webpay_settings_form($settings = NULL) {
  $form = array();

  $form['product_id'] = array(
    '#type' => 'textfield',
    '#title' => t('WebPay Product ID'),
    '#description' => t('The Product ID provided by Interswitch. Used to uniquely identify the receiving account.'),
    '#default_value' => isset($settings['product_id']) ? $settings['product_id'] : '',
    '#required' => TRUE,
  );

  $form['pay_item_id'] = array(
    '#type' => 'textfield',
    '#title' => t('WebPay Payment Item ID'),
    '#description' => t('The Payment Item ID provided by Interswitch.'),
    '#default_value' => isset($settings['pay_item_id']) ? $settings['pay_item_id'] : '',
    '#required' => TRUE,
  );

  $form['mac_key'] = array(
    '#type' => 'textfield',
    '#title' => t('WebPay Mac Key'),
    '#description' => t('The code used to secure the information sent from this website. 128 characters long.'),
    '#default_value' => isset($settings['mac_key']) ? $settings['mac_key'] : '',
    '#required' => TRUE,
  );

  return $form;
}









/**
 * Payment method callback: redirect form, a wrapper around the module's
 *   general use function for building a WebPay Website Payments form.
 */
function commerce_webpay_redirect_form($form, &$form_state, $order, $payment_method) {

  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $settings = $payment_method['settings'];

  $currency_code = $wrapper->commerce_order_total->currency_code->value(); // eg. 'USD' for US dollars
  $amount_raw = $wrapper->commerce_order_total->amount->value(); // eg. 100 for $1.00
  // $amount = round(commerce_currency_amount_to_decimal($amount_raw, $currency_code), 2);

  // See official ISO 4217 currency codes
  $currency_code_webpay = '566'; // 566 is the Naira

  // @todo fix this; it's not working
  // $all_currencies = module_invoke('commerce', 'commerce_commerce_currency_info'); // [$currency_code]['numeric_code'];
  // dpm($all_currencies, "$all_currencies");
  // $currency_code_webpay = $all_currencies[$currency_code]['numeric_code'];


  $return_url = url('commerce_webpay/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE));

  $item_name = t('Order @order_number at @store', array('@order_number' => $order->order_number, '@store' => variable_get('site_name', url('<front>', array('absolute' => TRUE)))));

  $product_name = t('product-count-!count', array('!count' => commerce_line_items_quantity($wrapper->commerce_line_items, commerce_product_line_item_types())));

  // Build the data array that will be translated into hidden form values.
  $data = array(
    'product_id' =>         $settings['product_id'], // $product_name,
    'amount' =>             $amount_raw,
    'currency' =>           $currency_code_webpay,
    'site_redirect_url' =>  $return_url,
    'txn_ref' =>            $order->order_id . '-' . REQUEST_TIME,
    'hash' =>               '',
    'pay_item_id' =>        $settings['pay_item_id'], // $order->order_id,

    // OPTIONAL fields
    'site_name' =>          variable_get('site_name'),
    // 'cust_id' =>            '',
    // 'cust_id_desc' =>       '',
    // 'cust_name' =>          '',
    // 'cust_name_desc' =>     '',
    'pay_item_name' =>      $item_name,
    'local_date_time' =>    REQUEST_TIME, // date("d-M-y H-m-s", REQUEST_TIME),
  );

  // // Give other modules a chance to alter the payment settings before they're sent off.
  // $hook = 'commerce_webpay_website_payment_settings';
  // foreach (module_implements($hook) as $module) {
  //   $function = $module . '_' . $hook;
  //   $function($data, $order, $payment_method);
  // }


  // Generate SHA512 security signature
  $signature = array();
  $mac_key = $settings['mac_key'];
  
  $signature = $data['txn_ref'] .
    $data['product_id'] .
    $data['pay_item_id'] .
    $data['amount'] .
    $data['site_redirect_url'] .
    $mac_key;

  $data['hash'] = hash('sha512', $signature);


  $form['#action'] = "https://stageserv.interswitchng.com/test_paydirect/pay";
  $form['#attributes'] = array('name' => 'frmPay');

  // Turn the data variables into form fields
  foreach ($data as $name => $value) {
    if (!empty($value)) {
      $form[$name] = array('#type' => 'hidden', '#value' => $value);
    }
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Proceed to WebPay'),
  );

  return $form;
}









// function commerce_webpay_redirect_form_validate() {
//   dpm("redirect_form_validate()");
// }

// function commerce_webpay_redirect_form_submit() {
//   dpm("redirect_form_submit()");
// }

/**
 * Processes an data on customer return from WebPay
 *
 * @param $payment_method
 *   The payment method instance array that originally made the payment.
 *
 * @return
 *   TRUE or FALSE indicating if the ITN was successfully processed.
 */
function commerce_webpay_process_return($order_id, $payment_redirect_key) {

  $params; // Stores the information sent back by WebPay

  // If the page was not called using the POST method
  if (empty($_POST) && empty($_GET)) {
    return array(
      '#markup' => "<p>No response was sent.</p>",
    );
  } elseif (!empty($_POST)) {
    $params = $_POST;
  } elseif (!empty($_GET)) {
    $params = $_GET;
  }

  // Prepare needed variables
  $order = commerce_order_load($order_id);
  $wrapper = entity_metadata_wrapper('commerce_order', $order);

  $currency_code = $wrapper->commerce_order_total->currency_code->value(); // eg. 'USD' for US dollars
  $amount_raw = $wrapper->commerce_order_total->amount->value(); // eg. 100 for $1.00
  // $amount = round(commerce_currency_amount_to_decimal($amount_raw, $currency_code), 2);

  $commerce_customer = commerce_customer_profile_load($order->commerce_customer_billing['und'][0]['profile_id']);
  // $account = user_load($commerce_customer->uid);

  $response = isset($params['resp']) ?      $params['resp'] :   "" ;
  $response_desc = isset($params['desc']) ? $params['desc'] :   "" ;
  $tax_ref = isset($params['txnref']) ?     $params['txnref'] : "" ;
  $output = "";
 
  // Provide a better-formatted response description if response code is recognised
  // switch ($params['resp']) {
  //   case 'Z4':
  //     $response_desc = "Mac validation failed";
  //     break;
  //   default:
  //     // $response_desc = "Error not recognised";
  //     break;
  // }

  // Payment failed
  if ($response) {
    $output .= "<h2>Payment Unsucessful</h2>"
      . "<p><strong>Error " . $response . "</strong></p>"
      . "<p>Description: " . $response_desc . "</p>"
      . "<p>Tax reference number " . $tax_ref . "</p>";

    // Log a 'failure' Drupal system message
    watchdog(
      'commerce_webpay', 
      'Payment failed with error @code: @error', 
      array('@code' => $response, '@error' => $response_desc), 
      WATCHDOG_ERROR);

    // commerce_payment_redirect_pane_previous_page($order);

  } 

  // Payment successful
  else {

    // Update the order status to checkout_complete (triggers order completion)
    $order = commerce_order_status_update($order, 'checkout_complete'); // , FALSE, TRUE, t('Order state updated by WebPay module.'));
    commerce_checkout_complete($order);

    $pay_ref = isset($params['payRef']) ? $params['payRef'] : "" ;

    // Save a new payment transaction for the order
    // a transaction is called a 'payment' in the drupal UI
    $transaction = commerce_payment_transaction_new('commerce_webpay', $order_id);

    // $transaction->instance_id = $tax_ref;
    $transaction->uid = $commerce_customer->uid;  
    $transaction->remote_id = $pay_ref;
    $transaction->remote_status = $response;
    $transaction->amount = $amount_raw; // @todo this should be a value from $params
    $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
    $transaction->message = t('The payment has been processed successfully.');

    commerce_payment_transaction_save($transaction);

    // Question: Does this trigger rule 'paid in full'? (if the payment >= the order total)

    // if ($params['amount'] >= $amount_raw) {
    //   invoke paid in full
    //   // Invoke the event including a hook of the same name.
    //   rules_invoke_all('commerce_payment_order_paid_in_full', $order, $transaction);
    //   // Update the order's data array to indicate this just happened.
    //   $order->data['commerce_payment_order_paid_in_full_invoked'] = TRUE;
    // }

    // Display a success message to the visitor
    $output .= "<h2>Payment Sucessful!</h2>";
    $output .= "<p>Your booking number is <strong>" . $order_id . "</strong></p>";
    $output .= "<p>Your tax reference number is <strong>" . $tax_ref . "</strong></p>";
    $output .= "<p>You have been sent an email with your booking information.</p>";

    // Log a 'success' Drupal system message
    watchdog(
      'commerce_webpay', 
      'Payment processed for Order @order_number with ID @txn_id.', 
      array('@txn_id' => $tax_ref, '@order_number' => $order_id), 
      WATCHDOG_INFO);

  }

  $output .= "<p>Return to " . l(t('Home'), '<front>', array('attributes' => array('class' => 'button'))) . "</p>";

  return array(
    '#markup' => $output,
  );
}









/**
 * Creates a rule that updates an order's status to 'complete' when paid in full
 */
// function 